@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
          <div class="col-md-5">
            <div class="panel panel-default">
                <div class="panel-heading">Bienvenido</div>
                <div class="panel-body">
                  Te damos la bienvenida al nuevo sistema del Club Andino Piramide.
                </div>
            </div>
          </div>
    </div>
</div>
@endsection
